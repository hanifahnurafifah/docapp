﻿using System;
using System.Collections.Generic;
using DocApp.Data.Entities;
using DocApp.Service.Interfaces;
using System.Linq;

namespace DocApp.Service
{
    public class DoctorsServiceService : BaseService, IDoctorsServiceService
    {
        public int AddNew(DoctorsService data)
        {
            DataContext.DoctorsServices.Add(data);
            DataContext.SaveChanges();
            return data.DoctorServiceId;
        }

        public void Delete(int id)
        {
            var data = new DoctorsService { DoctorServiceId = id };
            DataContext.DoctorsServices.Attach(data);
            DataContext.DoctorsServices.Remove(data);
            DataContext.SaveChanges();
        }

        public IEnumerable<DoctorsService> GetByDoctorId(int doctorId)
        {
            return DataContext.DoctorsServices.Where(x => x.DoctorsProfileId == doctorId);
        }

        public void SaveEdit(DoctorsService data)
        {
            var dataFromDB = DataContext.DoctorsServices.FirstOrDefault(x => x.DoctorServiceId == data.DoctorServiceId);
            dataFromDB.PriceRange = data.PriceRange;
            dataFromDB.ServiceName = data.ServiceName;
            DataContext.SaveChanges();
        }

        public DoctorsService Single(int id)
        {
            return DataContext.DoctorsServices.SingleOrDefault(x => x.DoctorServiceId == id);
        }
    }
}
