﻿using DocApp.Data.Entities;
using DocApp.Service.Interfaces;
using System.Linq;
using System;
using System.Collections.Generic;

namespace DocApp.Service
{
    public class UserProfileService : BaseService, IUserProfileService
    {
        public void AddNew(UsersProfile data)
        {
            DataContext.UsersProfiles.Add(data);
            DataContext.SaveChanges();
        }

        public void Delete(int id)
        {
            var data = new UsersProfile { UsersProfileId = id };
            DataContext.UsersProfiles.Attach(data);
            DataContext.UsersProfiles.Remove(data);
            DataContext.SaveChanges();
        }

        public IEnumerable<UsersProfile> GetAll()
        {
            return DataContext.UsersProfiles;
        }

        public void SaveEdit(UsersProfile data)
        {
            var dataFromDB = DataContext.UsersProfiles.FirstOrDefault(x => x.UsersProfileId == data.UsersProfileId);
            dataFromDB.Address = data.Address;
            dataFromDB.Avatar = data.Avatar;
            dataFromDB.AvatarContentType = data.AvatarContentType;
            dataFromDB.AvatarData = data.AvatarData;
            dataFromDB.AvatarPath = data.AvatarPath;
            dataFromDB.BirthDate = data.BirthDate;
            dataFromDB.FirstName = data.FirstName;
            dataFromDB.Gender = data.Gender;
            dataFromDB.LastName = data.LastName;
            DataContext.SaveChanges();
        }

        public UsersProfile SingleById(int id)
        {
            return DataContext.UsersProfiles.SingleOrDefault(x => x.UsersProfileId == id);
        }

        public UsersProfile SingleByUserId(string userId)
        {
            return DataContext.UsersProfiles.SingleOrDefault(x => x.UserId == userId);
        }
    }
}
