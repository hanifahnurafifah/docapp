﻿using System;
using System.Collections.Generic;
using DocApp.Data.Entities;
using DocApp.Service.Interfaces;
using System.Linq;

namespace DocApp.Service
{
    public class DoctorsScheduleService : BaseService, IDoctorsScheduleService
    {
        public void AddNew(DoctorsSchedule data)
        {
            DataContext.DoctorsSchedules.Add(data);
            DataContext.SaveChanges();
        }

        public void Delete(int id)
        {
            var data = new DoctorsSchedule { DoctorsScheduleId = id };
            DataContext.DoctorsSchedules.Attach(data);
            DataContext.DoctorsSchedules.Remove(data);
            DataContext.SaveChanges();
        }

        public IEnumerable<DoctorsSchedule> GetByDoctorId(int doctorId)
        {
            return DataContext.DoctorsSchedules.Where(x => x.DoctorsProfileId == doctorId);
        }

        public IEnumerable<DoctorsSchedule> GetByUserId(string userId)
        {
            return DataContext.DoctorsSchedules.Where(x => x.DoctorsProfile.UserId == userId);
        }

        public void SaveEdit(DoctorsSchedule data)
        {
            var dataFromDB = DataContext.DoctorsSchedules.FirstOrDefault(x => x.DoctorsScheduleId == data.DoctorsScheduleId);
            dataFromDB.Description = data.Description;
            dataFromDB.IsOpen = data.IsOpen;
            dataFromDB.Option = data.Option;
            dataFromDB.Quota = data.Quota;
            dataFromDB.ScheduleDay = data.ScheduleDay;
            dataFromDB.ScheduleTimeStart = data.ScheduleTimeStart;
            dataFromDB.ScheduleTimeEnd = data.ScheduleTimeEnd;
            DataContext.SaveChanges();
        }

        public DoctorsSchedule Single(int id)
        {
            return DataContext.DoctorsSchedules.SingleOrDefault(x => x.DoctorsScheduleId == id);
        }
    }
}
