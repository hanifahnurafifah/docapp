﻿using System;
using System.Collections.Generic;
using DocApp.Data.Entities;
using DocApp.Service.Interfaces;

namespace DocApp.Service
{
    public class RegisteredPatientService : BaseService, IRegisteredPatientService
    {
        public void AddNew(RegisteredPatient data)
        {
            throw new NotImplementedException();
        }

        public void Delete(int id)
        {
            throw new NotImplementedException();
        }

        public IEnumerable<RegisteredPatient> GetByScheduleId(int scheduleId)
        {
            throw new NotImplementedException();
        }

        public void SaveEdit(RegisteredPatient data)
        {
            throw new NotImplementedException();
        }

        public RegisteredPatient Single(int id)
        {
            throw new NotImplementedException();
        }
    }
}
