﻿using System;
using System.Collections.Generic;
using DocApp.Data.Entities;
using DocApp.Service.Interfaces;

namespace DocApp.Service
{
    public class SpecializationService : BaseService, ISpecializationService
    {
        public void AddNew(Specialization data)
        {
            throw new NotImplementedException();
        }

        public void Delete(int id)
        {
            throw new NotImplementedException();
        }

        public IEnumerable<Specialization> GetAll()
        {
            return DataContext.Specializations;
        }

        public void SaveEdit(Specialization data)
        {
            throw new NotImplementedException();
        }

        public IEnumerable<Specialization> SearchByKey(string key)
        {
            throw new NotImplementedException();
        }

        public Specialization SingleById(int id)
        {
            throw new NotImplementedException();
        }
    }
}
