﻿using DocApp.Data.Entities;
using System.Collections.Generic;

namespace DocApp.Service.Interfaces
{
    public interface IDoctorsScheduleService
    {
        void AddNew(DoctorsSchedule data);
        void SaveEdit(DoctorsSchedule data);
        DoctorsSchedule Single(int id);
        IEnumerable<DoctorsSchedule> GetByDoctorId(int doctorId);
        IEnumerable<DoctorsSchedule> GetByUserId(string userId);
        void Delete(int id);
    }
}
