﻿using DocApp.Data.Entities;
using System.Collections.Generic;

namespace DocApp.Service.Interfaces
{
    public interface IDoctorsServiceService
    {
        int AddNew(DoctorsService data);
        void SaveEdit(DoctorsService data);
        void Delete(int id);
        DoctorsService Single(int id);
        IEnumerable<DoctorsService> GetByDoctorId(int doctorId);
    }
}
