﻿using DocApp.Data;
using DocApp.Data.Entities;
using System.Collections.Generic;

namespace DocApp.Service.Interfaces
{
    public interface IDoctorsProfileService
    {
        void AddNew(DoctorsProfile data);
        void SaveEdit(DoctorsProfile data);
        DoctorsProfile SingleById(int id);
        DoctorsProfile SingleByUserId(string userId);
        void Delete(int id);
        IEnumerable<DoctorsProfile> GetDoctors();
    }
}
