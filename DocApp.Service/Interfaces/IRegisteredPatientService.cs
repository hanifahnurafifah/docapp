﻿using DocApp.Data.Entities;
using System.Collections.Generic;

namespace DocApp.Service.Interfaces
{
    public interface IRegisteredPatientService
    {
        void AddNew(RegisteredPatient data);
        void SaveEdit(RegisteredPatient data);
        RegisteredPatient Single(int id);
        IEnumerable<RegisteredPatient> GetByScheduleId(int scheduleId);
        void Delete(int id);
    }
}
