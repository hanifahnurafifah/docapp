﻿using DocApp.Data.Entities;
using System.Collections.Generic;

namespace DocApp.Service.Interfaces
{
    public interface ISpecializationService
    {
        void AddNew(Specialization data);
        void SaveEdit(Specialization data);
        Specialization SingleById(int id);
        IEnumerable<Specialization> SearchByKey(string key);
        IEnumerable<Specialization> GetAll();
        void Delete(int id);
    }
}
