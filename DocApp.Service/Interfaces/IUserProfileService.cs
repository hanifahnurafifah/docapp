﻿using DocApp.Data.Entities;
using System.Collections.Generic;

namespace DocApp.Service.Interfaces
{
    public interface IUserProfileService
    {
        void AddNew(UsersProfile data);
        void SaveEdit(UsersProfile data);
        UsersProfile SingleById(int id);
        UsersProfile SingleByUserId(string userId);
        void Delete(int id);
        IEnumerable<UsersProfile> GetAll();
    }
}
