﻿using DocApp.Data.Persistence;
using StructureMap;

namespace DocApp.Service
{
    public class BaseService
    {
        protected IDataContext DataContext
        {
            get { return ObjectFactory.GetInstance<IDataContext>(); }
        }
    }
}
