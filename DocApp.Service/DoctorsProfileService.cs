﻿using System;
using System.Collections.Generic;
using DocApp.Data.Entities;
using DocApp.Service.Interfaces;
using DocApp.Data;
using System.Linq;

namespace DocApp.Service
{
    public class DoctorsProfileService : BaseService, IDoctorsProfileService
    {
        public void AddNew(DoctorsProfile data)
        {
            DataContext.DoctorsProfiles.Add(data);
            DataContext.SaveChanges();
        }

        public void Delete(int id)
        {
            throw new NotImplementedException();
        }

        public IEnumerable<DoctorsProfile> GetDoctors()
        {
            return DataContext.DoctorsProfiles;
        }

        public void SaveEdit(DoctorsProfile data)
        {
            var dataFromDB = DataContext.DoctorsProfiles.FirstOrDefault(x => x.DoctorsProfileId == data.DoctorsProfileId);
            dataFromDB.Avatar = data.Avatar;
            dataFromDB.AvatarContentType = data.AvatarContentType;
            dataFromDB.AvatarData = data.AvatarData;
            dataFromDB.AvatarPath = data.AvatarPath;
            dataFromDB.FullName = data.FullName;
            dataFromDB.Gender = data.Gender;
            dataFromDB.AreaKey = data.AreaKey;
            dataFromDB.ClinicAddress = data.ClinicAddress;
            dataFromDB.Latitude = data.Latitude;
            dataFromDB.Longitude = data.Longitude;
            DataContext.SaveChanges();
        }

        public DoctorsProfile SingleById(int id)
        {
            return DataContext.DoctorsProfiles.SingleOrDefault(x => x.DoctorsProfileId == id);
        }

        public DoctorsProfile SingleByUserId(string userId)
        {
            return DataContext.DoctorsProfiles.SingleOrDefault(x => x.UserId == userId);
        }
    }
}
