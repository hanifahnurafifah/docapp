﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="IoC.cs" company="Web Advanced">
// Copyright 2012 Web Advanced (www.webadvanced.com)
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0

// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------


using DocApp.Data.Persistence;
using DocApp.Models;
using DocApp.Service;
using DocApp.Service.Interfaces;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.Owin.Security;
using StructureMap;
using StructureMap.Graph;
using StructureMap.Web.Pipeline;
using System.Data.Entity;
using System.Web;

namespace DocApp.DependencyResolution
{
    public static class IoC
    {
        public static IContainer Initialize()
        {
            ObjectFactory.Initialize(x =>
            {
                x.Scan(scan =>
                {
                    scan.TheCallingAssembly();
                    scan.WithDefaultConventions();
                });
                x.For<IDataContext>().LifecycleIs<HttpContextLifecycle>().Use<DataContext>();
                x.For<DbContext>().Use(() => new ApplicationDbContext());
                x.For<IUserStore<ApplicationUser>>().Use<UserStore<ApplicationUser>>();
                x.For<IAuthenticationManager>().Use(() => HttpContext.Current.GetOwinContext().Authentication);
                x.For<ISpecializationService>().Use<SpecializationService>();
                x.For<IDoctorsProfileService>().Use<DoctorsProfileService>();
                x.For<IDoctorsServiceService>().Use<DoctorsServiceService>();
                x.For<IDoctorsScheduleService>().Use<DoctorsScheduleService>();
                x.For<IRegisteredPatientService>().Use<RegisteredPatientService>();
                x.For<IUserProfileService>().Use<UserProfileService>();
            });

            return ObjectFactory.Container;
        }
    }
}