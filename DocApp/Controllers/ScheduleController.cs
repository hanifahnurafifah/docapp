﻿using DocApp.AutoMapper;
using DocApp.Data.Entities;
using DocApp.Service.Interfaces;
using DocApp.ViewModels;
using Microsoft.AspNet.Identity;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;

namespace DocApp.Controllers
{
    public class ScheduleController : Controller
    {
        private IDoctorsScheduleService _doctorsScheduleService;
        private IDoctorsProfileService _doctorsProfileService;

        public ScheduleController(IDoctorsScheduleService doctorsScheduleService,
            IDoctorsProfileService doctorsProfileService)
        {
            _doctorsScheduleService = doctorsScheduleService;
            _doctorsProfileService = doctorsProfileService;
        }

        // GET: Schedule
        public ActionResult Index()
        {
            var userId = User.Identity.GetUserId();
            var viewModel = new List<DoctorsScheduleListViewModel>();
            var data = _doctorsScheduleService.GetByUserId(userId).ToList();
            viewModel = data.MapTo<DoctorsScheduleListViewModel>();
            return View(viewModel);
        }

        public ActionResult Add()
        {
            var userId = User.Identity.GetUserId();
            var viewModel = new DoctorsScheduleViewModel();
            viewModel.Days = new List<SelectListItem>();
            viewModel.Days.Add(new SelectListItem { Text = "Senin", Value = "Senin" });
            viewModel.Days.Add(new SelectListItem { Text = "Selasa", Value = "Selasa" });
            viewModel.Days.Add(new SelectListItem { Text = "Rabu", Value = "Rabu" });
            viewModel.Days.Add(new SelectListItem { Text = "Kamis", Value = "Kamis" });
            viewModel.Days.Add(new SelectListItem { Text = "Jumat", Value = "Jumat" });
            viewModel.Days.Add(new SelectListItem { Text = "Sabtu", Value = "Sabtu" });
            viewModel.Days.Add(new SelectListItem { Text = "Minggu", Value = "Minggu" });
            viewModel.DoctorsProfileId = _doctorsProfileService.SingleByUserId(userId).DoctorsProfileId;
            return View(viewModel);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Add(DoctorsScheduleViewModel viewModel)
        {
            viewModel.Days = new List<SelectListItem>();
            viewModel.Days.Add(new SelectListItem { Text = "Senin", Value = "Senin" });
            viewModel.Days.Add(new SelectListItem { Text = "Selasa", Value = "Selasa" });
            viewModel.Days.Add(new SelectListItem { Text = "Rabu", Value = "Rabu" });
            viewModel.Days.Add(new SelectListItem { Text = "Kamis", Value = "Kamis" });
            viewModel.Days.Add(new SelectListItem { Text = "Jumat", Value = "Jumat" });
            viewModel.Days.Add(new SelectListItem { Text = "Sabtu", Value = "Sabtu" });
            viewModel.Days.Add(new SelectListItem { Text = "Minggu", Value = "Minggu" });

            try
            {
                var dataInput = viewModel.MapTo<DoctorsSchedule>();
                _doctorsScheduleService.AddNew(dataInput);
            }
            catch (System.Exception)
            {
                return View(viewModel);
            }
            return RedirectToAction("Index");
        }

        [HttpPost]
        public JsonResult Delete(int id)
        {
            try
            {
                _doctorsScheduleService.Delete(id);
                return Json(new { success = true });
            }
            catch (System.Exception)
            {
                return Json(new { success = false });
            }
        }
    }
}