﻿using DocApp.AutoMapper;
using DocApp.Data.Entities;
using DocApp.Helpers;
using DocApp.Service.Interfaces;
using DocApp.ViewModels;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Data.Entity.Spatial;
using System.Globalization;
using GoogleMaps.LocationServices;
using System;
using System.Net;
using System.Net.Sockets;
using System.Xml.Linq;
using System.Web.Script.Serialization;

namespace DocApp.Controllers
{
    [Authorize]
    public class ProfileController : Controller
    {
        private IUserProfileService _userProfileService;
        private IDoctorsProfileService _doctorProfileService;
        private ApplicationUserManager _userManager;
        private IDoctorsServiceService _doctorsServiceService;

        public ProfileController(IUserProfileService userProfileService,
            ApplicationUserManager userManager,
            IDoctorsProfileService doctorProfileService,
            IDoctorsServiceService doctorsServiceService)
        {
            _userProfileService = userProfileService;
            UserManager = userManager;
            _doctorProfileService = doctorProfileService;
            _doctorsServiceService = doctorsServiceService;
        }

        public ApplicationUserManager UserManager
        {
            get
            {
                return _userManager ?? HttpContext.GetOwinContext().GetUserManager<ApplicationUserManager>();
            }
            private set
            {
                _userManager = value;
            }
        }

        public ActionResult Index()
        {
            if (User.IsInRole("Doctor"))
            {
                return RedirectToAction("ProfileDoctor");
            }
            else if (User.IsInRole("Administrator"))
            {
                return RedirectToAction("Index", "Administrator");
            }
            return RedirectToAction("ProfileUser");
        }

        [Authorize(Roles = "User")]
        public ActionResult ProfileUser()
        {
            var viewModel = new UserProfileViewModel();
            var userId = User.Identity.GetUserId();
            var profile = _userProfileService.SingleByUserId(userId);
            viewModel = profile.MapTo<UserProfileViewModel>();
            viewModel.PhoneNumber = _userManager.GetPhoneNumber(userId);
            viewModel.Email = User.Identity.GetUserName();
            return View(viewModel);
        }

        [Authorize(Roles = "User")]
        public ActionResult EditUser(int id)
        {
            var viewModel = new UserProfileViewModel();
            var userId = User.Identity.GetUserId();
            var profile = _userProfileService.SingleByUserId(userId);
            viewModel = profile.MapTo<UserProfileViewModel>();
            viewModel.PhoneNumber = _userManager.GetPhoneNumber(userId);
            viewModel.Email = User.Identity.GetUserName();
            return View(viewModel);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult EditUser(UserProfileViewModel viewModel)
        {
            var dataFromDb = _userProfileService.SingleById(viewModel.UsersProfileId);
            dataFromDb = viewModel.MapTo<UsersProfile>();
            if (viewModel.FileAvatar != null)
            {
                if (System.IO.File.Exists(dataFromDb.AvatarData))
                {
                    System.IO.File.Delete(dataFromDb.AvatarPath);
                }

                var userId = User.Identity.GetUserId();
                var fileName = Path.GetFileName(viewModel.FileAvatar.FileName);
                var serverPath = Server.MapPath("~/Avatars/" + userId);
                var filePath = Path.Combine(serverPath, fileName);
                if (!Directory.Exists(serverPath))
                {
                    Directory.CreateDirectory(serverPath);
                }

                viewModel.FileAvatar.SaveAs(filePath);

                dataFromDb.AvatarPath = filePath;
                dataFromDb.Avatar = fileName;
                dataFromDb.AvatarData = DocAppHelper.GetSHA1HashFromFile(filePath);
                dataFromDb.AvatarContentType = viewModel.FileAvatar.ContentType;
            }
            _userProfileService.SaveEdit(dataFromDb);

            return RedirectToAction("Index");
        }

        public PartialViewResult Avatar()
        {
            var avatar = "";
            if (User.IsInRole("Doctor"))
            {
                avatar = _doctorProfileService.SingleByUserId(User.Identity.GetUserId()).Avatar;
            }
            else
            {
                avatar = _userProfileService.SingleByUserId(User.Identity.GetUserId()).Avatar;
            }
            return PartialView("_Avatar", avatar);
        }

        [Authorize(Roles = "Doctor")]
        public ActionResult ProfileDoctor()
        {
            var viewModel = DoctorsPushViewModel();
            return View(viewModel);
        }

        [Authorize(Roles = "Doctor")]
        public ActionResult EditDoctor(int id)
        {
            var viewModel = DoctorsPushViewModel();
            viewModel.Services = new List<DoctorsServiceList>();
            var services = _doctorsServiceService.GetByDoctorId(id);
            viewModel.Services = services.MapTo<DoctorsServiceList>();
            if (viewModel.Longitude == 0 && viewModel.Latitude == 0)
            {
                String url = "http://freegeoip.net/json/";
                using (WebClient client = new WebClient())
                {
                    string json = client.DownloadString(url);
                    Location location = new JavaScriptSerializer().Deserialize<Location>(json);
                    viewModel.Longitude = location.longitude;
                    viewModel.Latitude = location.latitude;
                }
            }
            
            return View(viewModel);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult EditDoctor(DoctorProfileViewModel viewModel)
        {
            var dataFromDb = _doctorProfileService.SingleById(viewModel.DoctorsProfileId);
            dataFromDb = viewModel.MapTo<DoctorsProfile>();
            if (viewModel.FileAvatar != null)
            {
                if (System.IO.File.Exists(dataFromDb.AvatarData))
                {
                    System.IO.File.Delete(dataFromDb.AvatarPath);
                }

                var userId = User.Identity.GetUserId();
                var fileName = Path.GetFileName(viewModel.FileAvatar.FileName);
                var serverPath = Server.MapPath("~/Avatars/" + userId);
                var filePath = Path.Combine(serverPath, fileName);
                if (!Directory.Exists(serverPath))
                {
                    Directory.CreateDirectory(serverPath);
                }

                viewModel.FileAvatar.SaveAs(filePath);

                dataFromDb.AvatarPath = filePath;
                dataFromDb.Avatar = fileName;
                dataFromDb.AvatarData = DocAppHelper.GetSHA1HashFromFile(filePath);
                dataFromDb.AvatarContentType = viewModel.FileAvatar.ContentType;
            }
            dataFromDb.GeoLocation = CreatePoint(viewModel.Latitude, viewModel.Longitude);
            _doctorProfileService.SaveEdit(dataFromDb);

            return RedirectToAction("Index");
        }

        public static DbGeography CreatePoint(double latitude, double longitude)
        {
            var text = string.Format(CultureInfo.InvariantCulture.NumberFormat,
            "POINT({0} {1})", longitude, latitude);
            // 4326 is most common coordinate system used by GPS/Maps
            return DbGeography.PointFromText(text, 4326);
        }

        public DoctorProfileViewModel DoctorsPushViewModel()
        {
            var viewModel = new DoctorProfileViewModel();
            var userId = User.Identity.GetUserId();
            var profile = _doctorProfileService.SingleByUserId(userId);
            viewModel = profile.MapTo<DoctorProfileViewModel>();
            viewModel.PhoneNumber = _userManager.GetPhoneNumber(userId);
            viewModel.Email = User.Identity.GetUserName();
            viewModel.Services = new List<DoctorsServiceList>();
            var services = _doctorsServiceService.GetByDoctorId(viewModel.DoctorsProfileId).ToList();
            if (services.Count > 0)
            {
                viewModel.Services = services.MapTo<DoctorsServiceList>();
            }
            return viewModel;
        }

        [HttpPost]
        public JsonResult SaveService(DoctorsServiceList model, int id)
        {
            var data = model.MapTo<DoctorsService>();
            data.DoctorsProfileId = id;
            var doctorsServiceId = model.DoctorServiceId;
            try
            {
                if (model.DoctorServiceId > 0)
                {
                    _doctorsServiceService.SaveEdit(data);
                }
                else
                {
                    doctorsServiceId = _doctorsServiceService.AddNew(data);
                }
                return Json(new { success = true, result = doctorsServiceId });
            }
            catch (System.Exception e)
            {
                return Json(new { success = false, result = e.Message });
            }
        }

        [HttpPost]
        public JsonResult DeleteService(int id)
        {
            try
            {
                _doctorsServiceService.Delete(id);
                return Json(new { success = true });
            }
            catch (System.Exception)
            {
                return Json(new { success = false });
            }
        }
    }
}