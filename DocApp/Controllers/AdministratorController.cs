﻿using DocApp.AutoMapper;
using DocApp.Data;
using DocApp.Data.Entities;
using DocApp.Models;
using DocApp.Service.Interfaces;
using DocApp.ViewModels;
using Microsoft.AspNet.Identity.Owin;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using System;
using PagedList;

namespace DocApp.Controllers
{
    public class AdministratorController : Controller
    {
        public const int PageSize = 10;
        public const int DefaultPage = 1;

        private ApplicationUserManager _userManager;
        private IDoctorsProfileService _doctorProfileService;
        private ISpecializationService _specializationService;
        private IUserProfileService _userProfileService;

        public AdministratorController(ApplicationUserManager userManager,
            IDoctorsProfileService doctorProfileService,
            ISpecializationService specializationService,
            IUserProfileService userProfileService)
        {
            UserManager = userManager;
            _doctorProfileService = doctorProfileService;
            _specializationService = specializationService;
            _userProfileService = userProfileService;
        }

        public ApplicationUserManager UserManager
        {
            get
            {
                return _userManager ?? HttpContext.GetOwinContext().GetUserManager<ApplicationUserManager>();
            }
            private set
            {
                _userManager = value;
            }
        }

        // GET: Administrator
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult RegisterDoctor()
        {
            var viewModel = new RegisterDoctorViewModel();
            viewModel.Specializations = new List<SelectListItem>();
            viewModel.Specializations = _specializationService.GetAll().Select(x => new SelectListItem { Value = x.SpecializationId.ToString(), Text = x.Name }).ToList();
            return View(viewModel);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> RegisterDoctor(RegisterDoctorViewModel model)
        {
            var user = new ApplicationUser { UserName = model.Email, Email = model.Email };
            var result = await UserManager.CreateAsync(user, model.Password);
            if (result.Succeeded)
            {
                var idManager = new IdentityManager();
                idManager.AddUserToRole(user.Id, "Doctor");

                var newDoctor = new DoctorsProfile();
                newDoctor.UserId = user.Id;
                newDoctor.SpecializationId = model.SpecializationId;
                newDoctor.FullName = model.FullName;
                _doctorProfileService.AddNew(newDoctor);
                return RedirectToAction("ListDoctor");
            }
            return View(model);
        }

        public ViewResult ListDoctor(string sortOrder, string currentFilter, string searchString, int? page)
        {
            ViewBag.GenderSortParm = String.IsNullOrEmpty(sortOrder) ? "gender_desc" : "";
            ViewBag.SpecializationSortParm = String.IsNullOrEmpty(sortOrder) ? "spec_desc" : "";

            if (searchString != null)
            {
                page = 1;
            }
            else
            {
                searchString = currentFilter;
            }

            ViewBag.CurrentFilter = searchString;

            var dataFromDb = _doctorProfileService.GetDoctors();
            var viewModel = new List<DoctorsListViewModel>();
            viewModel = dataFromDb.MapTo<DoctorsListViewModel>();
            if (!String.IsNullOrEmpty(searchString))
            {
                viewModel = viewModel.Where(s => s.FullName.ToLower().Contains(searchString.ToLower())).ToList();
            }
            switch (sortOrder)
            {
                case "gender_desc":
                    viewModel = viewModel.OrderByDescending(s => s.Gender).ToList();
                    break;
                case "spec_desc":
                    viewModel = viewModel.OrderByDescending(s => s.Specialization).ToList();
                    break;
                default:
                    viewModel = viewModel.OrderBy(s => s.FullName).ToList();
                    break;
            }
            
            int pageSize = 10;
            int pageNumber = (page ?? 1);
            return View(viewModel.ToPagedList(pageNumber, pageSize));
        }

        public ViewResult ListUser(string sortOrder, string currentFilter, string searchString, int? page)
        {
            ViewBag.GenderSortParm = String.IsNullOrEmpty(sortOrder) ? "gender_desc" : "";
            ViewBag.SpecializationSortParm = String.IsNullOrEmpty(sortOrder) ? "spec_desc" : "";

            if (searchString != null)
            {
                page = 1;
            }
            else
            {
                searchString = currentFilter;
            }

            ViewBag.CurrentFilter = searchString;

            var dataFromDb = _userProfileService.GetAll();
            var viewModel = new List<UserListViewModel>();
            //viewModel = dataFromDb.MapTo<UserListViewModel>();
            //if (!String.IsNullOrEmpty(searchString))
            //{
            //    viewModel = viewModel.Where(s => s.FullName.ToLower().Contains(searchString.ToLower())).ToList();
            //}
            //switch (sortOrder)
            //{
            //    case "gender_desc":
            //        viewModel = viewModel.OrderByDescending(s => s.Gender).ToList();
            //        break;
            //    case "spec_desc":
            //        viewModel = viewModel.OrderByDescending(s => s.Specialization).ToList();
            //        break;
            //    default:
            //        viewModel = viewModel.OrderBy(s => s.FullName).ToList();
            //        break;
            //}

            int pageSize = 10;
            int pageNumber = (page ?? 1);
            return View(viewModel.ToPagedList(pageNumber, pageSize));
        }
    }
}