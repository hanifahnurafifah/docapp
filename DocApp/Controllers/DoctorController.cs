﻿using DocApp.AutoMapper;
using DocApp.Service.Interfaces;
using DocApp.ViewModels;
using PagedList;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;

namespace DocApp.Controllers
{
    public class DoctorController : Controller
    {
        public const int PageSize = 10;
        public const int DefaultPage = 1;

        //private ApplicationUserManager _userManager;
        private IDoctorsProfileService _doctorProfileService;
        private IDoctorsScheduleService _doctorScheduleService;
        private ISpecializationService _specializationService;

        public DoctorController(/*ApplicationUserManager userManager,*/
            IDoctorsProfileService doctorProfileService,
            ISpecializationService specializationService,
            IDoctorsScheduleService doctorScheduleService)
        {
            //UserManager = userManager;
            _doctorProfileService = doctorProfileService;
            _specializationService = specializationService;
            _doctorScheduleService = doctorScheduleService;
        }

        // GET: Doctor
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult Search(SearchDoctorViewModel search, int? page)
        {
            var viewModel = new List<DoctorResultViewModel>();
            if (string.IsNullOrEmpty(search.Area) && string.IsNullOrEmpty(search.Day) &&
                string.IsNullOrEmpty(search.FullName) && search.Specialization == 0)
            {
                page = 1;
            }

            var dataFromDb = _doctorProfileService.GetDoctors();
            viewModel = dataFromDb.MapTo<DoctorResultViewModel>();
            foreach (var item in viewModel)
            {
                var scheduleDay = _doctorScheduleService.GetByDoctorId(item.DoctorsProfileId).ToList();
                if (scheduleDay.Count > 0)
                {
                    item.ScheduleDay = string.Join(",", scheduleDay.Select(x=>x.ScheduleDay));
                }
            }
            ViewBag.SearchArea = !string.IsNullOrEmpty(search.Area) ? search.Area.ToLower() : "";
            ViewBag.Day = !string.IsNullOrEmpty(search.Day) ? search.Day.ToLower() : "";
            ViewBag.SearchName = !string.IsNullOrEmpty(search.FullName) ? search.FullName.ToLower() : "";
            ViewBag.SearchSpecialization = search.Specialization;

            if (!string.IsNullOrEmpty(search.FullName))
            {
                viewModel = viewModel.Where(s => s.DoctorName.ToLower().Contains(search.FullName)).ToList();
            }
            if (!string.IsNullOrEmpty(search.Day))
            {
                viewModel = viewModel.Where(s => s.ScheduleDay.ToLower().Contains(search.Day)).ToList();
            }
            if (!string.IsNullOrEmpty(search.Area))
            {
                viewModel = viewModel.Where(s => s.Address.ToLower().Contains(search.Area) || s.Area.ToLower().Contains(search.Area)).ToList();
            }
            if (search.Specialization > 0)
            {
                viewModel = viewModel.Where(x => x.SpecializationId == search.Specialization).ToList();
            }
            int pageSize = 10;
            int pageNumber = (page ?? 1);
            return View(viewModel.ToPagedList(pageNumber, pageSize));
        }

        public JsonResult GetSpecialization()
        {
            var specialization = _specializationService.GetAll();
            List<object> returnJson = new List<object>();
            foreach (var item in specialization)
            {
                returnJson.Add(new { id = item.SpecializationId, name = item.Name });
            }
            return Json(returnJson, JsonRequestBehavior.AllowGet);
        }

        public ActionResult Detail(int id)
        {
            var viewModel = new DoctorDetailViewModel();
            var dataFromDb = _doctorProfileService.SingleById(id);
            viewModel = dataFromDb.MapTo<DoctorDetailViewModel>();
            viewModel.Schedules = new List<Schedule>();
            var schedules = _doctorScheduleService.GetByDoctorId(id);
            foreach (var item in schedules)
            {
                viewModel.Schedules.Add(new Schedule
                {
                    Day = item.ScheduleDay,
                    TimeSchedule = item.ScheduleTimeStart.ToString("hh:mm") + "-" + item.ScheduleTimeEnd.ToString("hh:mm"),
                    IsOpen = item.IsOpen
                });
            }
            return View(viewModel);
        }
    }
}