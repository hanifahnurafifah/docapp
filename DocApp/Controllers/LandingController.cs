﻿using DocApp.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace DocApp.Controllers
{
    public class LandingController : Controller
    {
        // GET: Landing
        public ActionResult Index()
        {
            var idManager = new IdentityManager();
            if (!idManager.RoleExists("Administrator"))
            {
                idManager.CreateRole("Administrator");
            }
            if (!idManager.RoleExists("User"))
            {
                idManager.CreateRole("User");
            }
            if (!idManager.RoleExists("Doctor"))
            {
                idManager.CreateRole("Doctor");
            }
            return View();
        }
    }
}