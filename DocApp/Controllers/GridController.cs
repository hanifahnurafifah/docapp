﻿using DocApp.Data;
using System.Collections.Generic;
using System.Web.Mvc;

namespace DocApp.Controllers
{
    public class GridController : Controller
    {
        // GET: Grid
        public List<FilterGrid> GetFilter(System.Collections.Specialized.NameValueCollection query)
        {
            var filter = new List<FilterGrid>();
            var filterCount = int.Parse(query.GetValues("filterscount")[0]);
            if (filterCount > 0)
            {
                for (var i = 0; i < filterCount; i += 1)
                {
                    filter.Add(new FilterGrid
                    {
                        FilterValue = query.GetValues("filtervalue" + i)[0],
                        FilterCondition = query.GetValues("filtercondition" + i)[0],
                        FilterDatafield = query.GetValues("filterdatafield" + i)[0],
                        FilterOperator = query.GetValues("filteroperator" + i)[0]
                    });
                }
            }
            return filter;
        }
    }
}