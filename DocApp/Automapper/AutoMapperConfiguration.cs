﻿using AutoMapper;
using DocApp.Data;
using DocApp.Data.Entities;
using DocApp.ViewModels;
using System;

namespace DocApp.Automapper
{
    public class AutoMapperConfiguration
    {
        public static void Configure()
        {
            Mapper.CreateMap<UsersProfile, UserProfileViewModel>()
                .ForMember(x => x.BirtDateString, o => o.MapFrom(y => y.BirthDate.HasValue ? y.BirthDate.Value.ToString("dd MMMM yyyy") : ""));
            Mapper.CreateMap<UserProfileViewModel, UsersProfile>();
            Mapper.CreateMap<DoctorsProfile, DoctorsListViewModel>()
                .ForMember(x => x.Specialization, o => o.MapFrom(y => y.Specialization.Name));
            Mapper.CreateMap<DoctorsProfile, DoctorProfileViewModel>()
                .ForMember(x => x.Specialization, o => o.MapFrom(y => y.Specialization.Name));
            Mapper.CreateMap<DoctorProfileViewModel, DoctorsProfile>()
                .ForMember(x => x.Specialization, o => o.Ignore());
            Mapper.CreateMap<DoctorsService, DoctorsServiceList>();
            Mapper.CreateMap<DoctorsServiceList, DoctorsService>();
            Mapper.CreateMap<DoctorsSchedule, DoctorsScheduleViewModel>()
                .ForMember(x => x.ScheduleTimeStart, o => o.MapFrom(y => y.ScheduleTimeStart.ToString("hh:mm")))
                .ForMember(x => x.ScheduleTimeStart, o => o.MapFrom(y => y.ScheduleTimeEnd.ToString("hh:mm")));
            Mapper.CreateMap<DoctorsScheduleViewModel, DoctorsSchedule>()
                .ForMember(x => x.ScheduleTimeStart, o => o.MapFrom(y => Convert.ToDateTime(y.ScheduleTimeStart)))
                .ForMember(x => x.ScheduleTimeStart, o => o.MapFrom(y => Convert.ToDateTime(y.ScheduleTimeEnd)));
            Mapper.CreateMap<DoctorsSchedule, DoctorsScheduleListViewModel>()
                .ForMember(x => x.ScheduleTime, o => o.MapFrom(y => y.ScheduleTimeStart.ToString("hh:mm") + "-" + y.ScheduleTimeEnd.ToString("hh:mm")));
            Mapper.CreateMap<DoctorsProfile, DoctorResultViewModel>()
                .ForMember(x => x.Specialization, o => o.MapFrom(y => y.Specialization.Name))
                .ForMember(x => x.DoctorName, o => o.MapFrom(y => y.FullName))
                .ForMember(x => x.Address, o => o.MapFrom(y => y.ClinicAddress));
            Mapper.CreateMap<DoctorsProfile, DoctorDetailViewModel>()
                .ForMember(x => x.Specialization, o => o.MapFrom(y => y.Specialization.Name));
        }
    }
}