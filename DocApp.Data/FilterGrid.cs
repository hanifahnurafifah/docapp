﻿namespace DocApp.Data
{
    public class FilterGrid
    {
        public string FilterValue { get; set; }
        public string FilterCondition { get; set; }
        public string FilterDatafield { get; set; }
        public string FilterOperator { get; set; }
    }
}
