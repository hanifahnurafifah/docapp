﻿using DocApp.Data.Entities;
using DocApp.Data.Persistence;

namespace DocApp.Data.Installer
{
    public class SpecializationInstaller
    {
        private readonly DataContext _dataContext;

        public SpecializationInstaller(DataContext dataContext)
        {
            _dataContext = dataContext;
        }

        public void Installer()
        {
            var data1 = new Specialization { Name = "Akupuntur" };
            var data2 = new Specialization { Name = "Anak" };
            var data3 = new Specialization { Name = "Anestesi" };
            var data4 = new Specialization { Name = "Andrologi" };
            var data5 = new Specialization { Name = "Bedah Anak" };
            var data6 = new Specialization { Name = "Bedah Digestive" };
            var data7 = new Specialization { Name = "Bedah Mulut" };
            var data8 = new Specialization { Name = "Bedah Plastik" };
            var data9 = new Specialization { Name = "Bedah Saluran Cerna" };
            var data10 = new Specialization { Name = "Bedah Saluran Kemih" };
            var data11 = new Specialization { Name = "Bedah Saraf" };
            var data12 = new Specialization { Name = "Bedah Thoraks" };
            var data13 = new Specialization { Name = "Bedah Tulang (Ortopedi & Traumatologi)" };
            var data14 = new Specialization { Name = "Bedah Tumor (Onkologi)" };
            var data15 = new Specialization { Name = "Bedah Umum" };
            var data16 = new Specialization { Name = "Bedah Urologi" };
            var data17 = new Specialization { Name = "Biopsi Jarum Halus" };
            var data18 = new Specialization { Name = "Dokter Umum" };
            var data19 = new Specialization { Name = "Gigi" };
            var data20 = new Specialization { Name = "Gigi Anak" };
            var data21 = new Specialization { Name = "Gizi" };
            var data22 = new Specialization { Name = "Jantung" };
            var data23 = new Specialization { Name = "Jiwa" };
            var data24 = new Specialization { Name = "Kandungan Kebidanan" };
            var data25 = new Specialization { Name = "Kulit dan Kelamin" };
            var data26 = new Specialization { Name = "Mata" };
            var data27 = new Specialization { Name = "Paru" };
            var data28 = new Specialization { Name = "Patologi Anatomi" };
            var data29 = new Specialization { Name = "Patologi Klinik" };
            var data30 = new Specialization { Name = "Penyakit Dalam" };
            var data31 = new Specialization { Name = "Radiologi" };
            var data32 = new Specialization { Name = "Radiotherapi" };
            var data33 = new Specialization { Name = "Rehabilitasi" };
            var data34 = new Specialization { Name = "Saraf" };
            var data35 = new Specialization { Name = "THT" };

            _dataContext.Specializations.Add(data1);
            _dataContext.Specializations.Add(data2);
            _dataContext.Specializations.Add(data3);
            _dataContext.Specializations.Add(data4);
            _dataContext.Specializations.Add(data5);
            _dataContext.Specializations.Add(data6);
            _dataContext.Specializations.Add(data7);
            _dataContext.Specializations.Add(data8);
            _dataContext.Specializations.Add(data9);
            _dataContext.Specializations.Add(data10);
            _dataContext.Specializations.Add(data11);
            _dataContext.Specializations.Add(data12);
            _dataContext.Specializations.Add(data13);
            _dataContext.Specializations.Add(data14);
            _dataContext.Specializations.Add(data15);
            _dataContext.Specializations.Add(data16);
            _dataContext.Specializations.Add(data17);
            _dataContext.Specializations.Add(data18);
            _dataContext.Specializations.Add(data19);
            _dataContext.Specializations.Add(data20);
            _dataContext.Specializations.Add(data21);
            _dataContext.Specializations.Add(data22);
            _dataContext.Specializations.Add(data23);
            _dataContext.Specializations.Add(data24);
            _dataContext.Specializations.Add(data25);
            _dataContext.Specializations.Add(data26);
            _dataContext.Specializations.Add(data27);
            _dataContext.Specializations.Add(data28);
            _dataContext.Specializations.Add(data29);
            _dataContext.Specializations.Add(data30);
            _dataContext.Specializations.Add(data31);
            _dataContext.Specializations.Add(data32);
            _dataContext.Specializations.Add(data33);
            _dataContext.Specializations.Add(data34);
            _dataContext.Specializations.Add(data35);
        }
    }
}
