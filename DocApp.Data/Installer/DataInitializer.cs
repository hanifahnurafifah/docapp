﻿using DocApp.Data.Installer;
using DocApp.Data.Persistence;
using System.Data.Entity;

namespace DocApp.Installer.Data
{
    public class DataInitializer : DropCreateDatabaseIfModelChanges<DataContext>
    {
        protected override void Seed(DataContext context)
        {
            var specialization = new SpecializationInstaller(context);
            specialization.Installer();

            context.SaveChanges();
            base.Seed(context);
        }
    }
}
