﻿using DocApp.Data.Entities;
using System.Data.Entity;

namespace DocApp.Data.Persistence
{
    public class DataContext : DbContext, IDataContext
    {
        public DataContext()
            : base("DefaultConnection")
        {
        }
        public IDbSet<Specialization> Specializations { get; set; }
        public IDbSet<UsersProfile> UsersProfiles { get; set; }
        public IDbSet<DoctorsProfile> DoctorsProfiles { get; set; }
        public IDbSet<DoctorsService> DoctorsServices { get; set; }
        public IDbSet<DoctorsSchedule> DoctorsSchedules { get; set; }
        public IDbSet<RegisteredPatient> RegisteredPatients { get; set; }
    }
}
