﻿using DocApp.Data.Entities;
using System;
using System.Data.Entity;

namespace DocApp.Data.Persistence
{
    public interface IDataContext : IDisposable
    {
        IDbSet<Specialization> Specializations { get; set; }
        IDbSet<UsersProfile> UsersProfiles { get; set; }
        IDbSet<DoctorsProfile> DoctorsProfiles { get; set; }
        IDbSet<DoctorsService> DoctorsServices { get; set; }
        IDbSet<DoctorsSchedule> DoctorsSchedules { get; set; }
        IDbSet<RegisteredPatient> RegisteredPatients { get; set; }
        Database Database { get; }
        int SaveChanges();
    }
}
