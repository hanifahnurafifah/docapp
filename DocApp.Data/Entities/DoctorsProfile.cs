﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.Spatial;

namespace DocApp.Data.Entities
{
    public class DoctorsProfile
    {
        [Key]
        [DatabaseGeneratedAttribute(DatabaseGeneratedOption.Identity)]
        public int DoctorsProfileId { get; set; }

        [StringLength(128)]
        public string UserId { get; set; }

        [StringLength(150)]
        public string FullName { get; set; }
        
        [ForeignKey("Specialization")]
        public int SpecializationId { get; set; }

        public string Avatar { get; set; }

        public string AvatarPath { get; set; }

        public string AvatarData { get; set; }

        public string AvatarContentType { get; set; }

        [StringLength(10)]
        public string Gender { get; set; }

        public string ClinicAddress { get; set; }

        public float Longitude { get; set; }

        public float Latitude { get; set; }
        
        public DbGeography GeoLocation { get; set; }

        public string AreaKey { get; set; }

        public virtual Specialization Specialization { get; set; }
    }
}
