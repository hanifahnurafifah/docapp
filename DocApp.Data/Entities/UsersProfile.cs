﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace DocApp.Data.Entities
{
    public class UsersProfile
    {
        [Key]
        [DatabaseGeneratedAttribute(DatabaseGeneratedOption.Identity)]
        public int UsersProfileId { get; set; }

        [StringLength(128)]
        public string UserId { get; set; }

        [StringLength(50)]
        public string FirstName { get; set; }

        [StringLength(50)]
        public string LastName { get; set; }

        public string Address { get; set; }

        public DateTime? BirthDate { get; set; }

        public string Avatar { get; set; }

        public string AvatarPath { get; set; }

        public string AvatarData { get; set; }

        public string AvatarContentType { get; set; }

        [StringLength(10)]
        public string Gender { get; set; }
    }
}
