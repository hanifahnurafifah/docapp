﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace DocApp.Data.Entities
{
    public class RegisteredPatient
    {
        [Key]
        [DatabaseGeneratedAttribute(DatabaseGeneratedOption.Identity)]
        public int RegisteredPatientId { get; set; }

        [ForeignKey("UsersProfile")]
        public int UsersProfileId { get; set; }

        [ForeignKey("DoctorsSchedule")]
        public int DoctorsScheduleId { get; set; }

        public int RegisteredNumber { get; set; }

        public int Rate { get; set; }

        public DateTime RegisterDate { get; set; }

        public bool IsCancel { get; set; }

        public bool IsFinished { get; set; }

        public virtual UsersProfile UsersProfile { get; set; }

        public virtual DoctorsSchedule DoctorsSchedule { get; set; }
    }
}
