﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace DocApp.Data.Entities
{
    public class DoctorsSchedule
    {
        [Key]
        [DatabaseGeneratedAttribute(DatabaseGeneratedOption.Identity)]
        public int DoctorsScheduleId { get; set; }

        [ForeignKey("DoctorsProfile")]
        public int DoctorsProfileId { get; set; }

        public string ScheduleDay { get; set; }

        public DateTime ScheduleTimeStart { get; set; }

        public DateTime ScheduleTimeEnd { get; set; }

        public bool IsOpen { get; set; }

        public string Description { get; set; }

        public int Quota { get; set; }

        [StringLength(50)]
        public string Option { get; set; }

        public virtual DoctorsProfile DoctorsProfile { get; set; }
    }
}
