﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace DocApp.Data.Entities
{
    public class DoctorsService
    {
        [Key]
        [DatabaseGeneratedAttribute(DatabaseGeneratedOption.Identity)]
        public int DoctorServiceId { get; set; }

        [ForeignKey("DoctorsProfile")]
        public int DoctorsProfileId { get; set; }

        [StringLength(100)]
        public string ServiceName { get; set; }

        public decimal PriceRange { get; set; }

        public virtual DoctorsProfile DoctorsProfile { get; set; }
    }
}
