﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace DocApp.Data.Entities
{
    public class Specialization
    {
        [Key]
        [DatabaseGeneratedAttribute(DatabaseGeneratedOption.Identity)]
        public int SpecializationId { get; set; }

        [StringLength(50)]
        public string Name { get; set; }
    }
}
